module gitlab.com/birowo/custmidware

go 1.22.3

require (
	github.com/klauspost/compress v1.17.8
	github.com/valyala/fasthttp v1.54.0
	gitlab.com/birowo/middleware v0.0.0-20240601020359-4cbc3d59a9d1
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
