package main

import (
	"bytes"
	"net/http"
	"strings"

	"github.com/klauspost/compress/zstd"
	"github.com/valyala/fasthttp"
	"gitlab.com/birowo/middleware"
)

type RW struct {
	http.ResponseWriter
	*zstd.Encoder
}

func (rw RW) Write(w []byte) (int, error) {
	return rw.Encoder.Write(w)
}
func main() {
	mw0 := func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			if ac := r.Header["Accept-Encoding"]; ac != nil && strings.Contains(ac[0], "zstd") {
				enc, err := zstd.NewWriter(w)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				defer enc.Close()
				w.Header().Set("Content-Encoding", "zstd")
				next(RW{w, enc}, r)
				return
			}
			next(w, r)
		}
	}
	mw1 := func(_ http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			w.Write(bytes.Repeat([]byte("qwertyuiopasdfghjklzxcvbnm<br>"), 100))
		}
	}
	go http.ListenAndServe(":8080", middleware.Chain(mw0, mw1))

	mwA := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			if bytes.Contains(ctx.Request.Header.Peek("Accept-Encoding"), []byte("zstd")) {
				ctx.Response.Header.Set("Content-Encoding", "zstd")
				enc, err := zstd.NewWriter(ctx)
				if err != nil {
					ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
					return
				}
				defer enc.Close()
				next(ctx)
				body := ctx.Response.Body()
				ctx.ResetBody()
				enc.Write(body)
				return
			}
			next(ctx)
		}
	}
	mwB := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			ctx.Response.Header.Set("Content-Type", "text/html; charset=utf-8")
			ctx.Write(bytes.Repeat([]byte("qwertyuiopasdfghjklzxcvbnm<br>"), 100))
		}
	}
	fasthttp.ListenAndServe(":8181", middleware.Chain(mwA, mwB))
}
